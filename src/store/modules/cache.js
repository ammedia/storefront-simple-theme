export default {

  namespaced: true,

  state: {
    cache: {}
  },

  mutations: {

    put(state, data) {
      state[data.key] = data.value;
    }
  }

};