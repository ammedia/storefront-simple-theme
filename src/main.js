import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Bootstrap
require('bootstrap');

import $ from 'jquery';
window.jQuery = $;
window.$ = $;

require('@/assets/nice-select/jquery.nice-select.min.js');
require('@/assets/nice-select/nice-select.css')

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
